class_name Chest extends StaticBody3D

signal signal_toggle_inventory(external_inventory_owner)

@export var inventory_data: InventoryData

func player_interact() -> void:
	signal_toggle_inventory.emit(self)
