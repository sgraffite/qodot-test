extends RigidBody3D
@export var fall_sounds: Array[Resource]
@onready var audio_stream_player_3d = $"../AudioStreamPlayer3D"
var fall_factor = 10

func _on_body_entered(_body):
	var x = abs(self.linear_velocity.x)
	var y = abs(self.linear_velocity.y)
	var z = abs(self.linear_velocity.z)
	
	#print("x" + str(x))
	#print("y" + str(y))
	#print("z" + str(z))
	if(x <= fall_factor && y <= fall_factor && z <= fall_factor):
		return
	
	audio_stream_player_3d.stream = fall_sounds.pick_random()
	audio_stream_player_3d.play()
