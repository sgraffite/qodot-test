extends Control


@onready var option_button = $HBoxContainer/OptionButton as Button
var initial_window_mode:int = ProjectSettings.get_setting("display/window/size/mode")
var initial_window_borderless:bool = ProjectSettings.get_setting("display/window/size/borderless")

const WINDOW_MODE_ARRAY : Array[String] = [
	"Full Screen",
	"Windowed",
	"Borderless Windowed",
	"Borderless Full Screen"
]

func _ready():
	add_window_mode_items()
	option_button.item_selected.connect(on_window_mode_selected)

func add_window_mode_items() -> void:
	for window_mode in WINDOW_MODE_ARRAY:
		option_button.add_item(window_mode)
	print("windowMode=", initial_window_mode, " borderless=", initial_window_borderless)
	option_button.selected = WINDOW_MODE_ARRAY.find("Windowed")

func on_window_mode_selected(index : int) -> void:
	match index:
		0: #Full Screen
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
			DisplayServer.window_set_flag(DisplayServer.WINDOW_FLAG_BORDERLESS, false)
		1: #Windowed
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
			DisplayServer.window_set_flag(DisplayServer.WINDOW_FLAG_BORDERLESS, false)
		2: #Borderless Windowed
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
			DisplayServer.window_set_flag(DisplayServer.WINDOW_FLAG_BORDERLESS, true)
		3: #Borderless Full Screen
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
			DisplayServer.window_set_flag(DisplayServer.WINDOW_FLAG_BORDERLESS, true)
