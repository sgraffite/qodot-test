class_name MainMenu extends Control

@onready var start_button = $MarginContainer/HBoxContainer/VBoxContainer/Start_Button as Button
@onready var options_button = $MarginContainer/HBoxContainer/VBoxContainer/Options_Button as Button
@onready var exit_button = $MarginContainer/HBoxContainer/VBoxContainer/Exit_Button as Button
@onready var margin_container = $MarginContainer as MarginContainer
@onready var options_menu = $Options_Menu
@onready var audio_stream_player_2d = $AudioStreamPlayer2D
const MENU_CLICK = preload("res://assets/sound/menu_click.mp3")
const MOCKINGBIRD = preload("res://assets/sound/birb/mockingbird.wav")

signal signal_game_start

func _ready():
	audio_stream_player_2d.stream = MOCKINGBIRD
	audio_stream_player_2d.play()
	handle_connecting_signals()
	
func on_start_pressed() -> void:
	audio_stream_player_2d.stream = MENU_CLICK
	audio_stream_player_2d.play()
	start_button.visible = false
	emit_signal("signal_game_start")

func on_options_pressed() -> void:
	audio_stream_player_2d.stream = MENU_CLICK
	audio_stream_player_2d.play()
	margin_container.visible = false
	options_menu.visible = true
	options_menu.set_process(true)

func on_exit_pressed() -> void:
	get_tree().quit()

func on_exit_options_menu() -> void:
	margin_container.visible = true
	options_menu.visible = false

func handle_connecting_signals() -> void:
	start_button.button_down.connect(on_start_pressed)
	options_button.button_down.connect(on_options_pressed)
	exit_button.button_down.connect(on_exit_pressed)
	options_menu.exit_options_menu.connect(on_exit_options_menu)
