extends Node3D
@export var bot_scenes: Array[PackedScene]
var player = null
var robot_index: int = 0
var spawn_delay: float = 2.0
@onready var spawn_bucket: Node3D = $SpawnBucket


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(!player):
		player = get_tree().get_root().get_node_or_null("Player")
		create_timer()

func create_timer():
	var timer = get_tree().create_timer(spawn_delay, false)
	timer.connect("timeout", self._on_timer_timeout)
	spawn_delay = clamp(spawn_delay / 1.2, .1, 3)

func _on_timer_timeout():
	var queued_bot: Node3D = bot_scenes[robot_index].instantiate()
	robot_index = robot_index + 1 if robot_index < bot_scenes.size()-1 else 0
	# Determine player facing by head rotation
	var player_forward_vector = player.head.global_transform.basis.z.normalized()
	var spawn_position = player.position
	spawn_position -= player_forward_vector * 3
	spawn_position.y = queued_bot.position.y + 20
	spawn_bucket.add_child(queued_bot)
	# Must set global position after adding the child to the tree
	queued_bot.global_transform.origin = spawn_position
	#queued_bot.look_at_from_position(queued_bot.position, player.position)
	queued_bot.look_at(player.position)
	create_timer()
	if(spawn_bucket.get_child_count() > 400):
		spawn_bucket.get_child(0).queue_free()
