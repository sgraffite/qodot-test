extends Node

@onready var main_menu = $MainMenu
@onready var inventory_interface = $UI/InventoryInterface as Control
@onready var hot_bar_inventory = $UI/HotBarInventory

@onready var queued_level = preload("res://levels/test_level.tscn") as PackedScene
@onready var BIRB_LEVEL = preload("res://levels/birb_level.tscn") as PackedScene
@onready var queued_player = preload("res://scenes/player/player.tscn") as PackedScene


const PICK_UP = preload("res://item/items/pick_up/pick_up.tscn")

var is_game_started:bool = false
var current_level:Node = null
var current_player:Node = null

func _ready():
	DisplayServer.window_set_title("Birbs Aren't Real")

func _input(_event) -> void:
	if(!is_game_started):
		return
	
	if(main_menu.visible):
		return
	
	if Input.is_action_just_pressed("escape"):
		on_escape_pressed()
	
	if Input.is_action_just_pressed("inventory"):
		toggle_inventory_interface()
	
	if Input.is_action_just_pressed("interact"):
		interact()

func on_escape_pressed() -> void:
	main_menu.visible = !main_menu.visible
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE if main_menu.visible else Input.MOUSE_MODE_CAPTURED)
	current_level.set_process(current_level.visible)
	
func _on_main_menu_signal_game_start() -> void:
	main_menu.visible = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	print("_on_main_menu_signal_game_start")
	# Create level
	current_level = BIRB_LEVEL.instantiate()
	get_tree().root.add_child(current_level)
	# Create player
	current_player = queued_player.instantiate()
	get_tree().root.add_child(current_player)
	# Create player inventory
	inventory_interface.set_player_inventory_data(current_player.inventory_data)
	inventory_interface.set_equip_inventory_data(current_player.equip_inventory_data)
	inventory_interface.signal_force_close.connect(toggle_inventory_interface)
	hot_bar_inventory.set_inventory_data(current_player.inventory_data)
	#hot_bar_inventory.show()
	# Register external_inventory group to callback signal_toggle_inventory
	for node in get_tree().get_nodes_in_group("external_inventory"):
		node.signal_toggle_inventory.connect(toggle_inventory_interface)
	is_game_started = true

func toggle_inventory_interface(external_inventory_owner = null):
	inventory_interface.set_visible(!inventory_interface.visible)
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE if inventory_interface.visible else Input.MOUSE_MODE_CAPTURED)
	#if(inventory_interface.visible):
		#hot_bar_inventory.hide()
	#else:
		#hot_bar_inventory.show()
	
	if(external_inventory_owner && inventory_interface.visible):
		inventory_interface.set_external_inventory(external_inventory_owner)
	else:
		inventory_interface.clear_external_inventory()
# TODO: move to player.gd?
func interact() -> void:
	if(current_player.interact_ray.is_colliding()):
		current_player.interact_ray.get_collider().player_interact()
		print("interact with", current_player.interact_ray.get_collider())
		return


func _on_inventory_interface_signal_drop_slot_data(slot_data) -> void:
	var pick_up = PICK_UP.instantiate()
	pick_up.slot_data = slot_data
	pick_up.position = current_player.get_drop_position()
	add_child(pick_up)
	print("_on_inventory_interface_signal_drop_slot_data")
