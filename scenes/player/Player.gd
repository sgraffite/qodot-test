extends CharacterBody3D

@onready var head = $Head as Node3D
@onready var camera = $Head/Camera3D as Camera3D
@onready var camera_3d_ortho = $Head/Camera3DOrtho as Camera3D
@onready var interact_ray = $Head/Camera3D/InteractRay as RayCast3D

@export var inventory_data: InventoryData
@export var equip_inventory_data: InventoryDataEquip

var hp = 5
var speed
const WALK_SPEED = 5.0
const SPRINT_SPEED = 8.0
var JUMP_VELOCITY = Vector3(0, 7, 0)
const MOUSE_SENSITIVITY = 0.003
const INVERT_MOUSE := 1		#1 or -1, 1 means yes
const CONTROLLER_X_SENSITIVITY = 0.2
const CONTROLLER_Y_SENSITIVITY = 0.1

#bob variables
const BOB_FREQ = 2.4
const BOB_AMP = 0.08
var t_bob = 0.0

#fov variables
const BASE_FOV = 75.0
const FOV_CHANGE = 1.5

# Get the gravity from the project settings to be synced with RigidBody nodes.
#var gravity = 9.8
var gravity = Vector3(0, 9.8, 0)


func _ready():
	PlayerManager.player = self
	camera.current = true
	set_up_direction(Vector3.UP)

func _input(event):
	if event is InputEventMouseButton:
		# Middle mouse button
		if(event.button_index == 3 && event.pressed == true):
			if(!camera.current):
				camera.current = true
			elif(!camera_3d_ortho.current):
				camera_3d_ortho.current = true
			
		# Scroll up (zoom in)
		if(event.button_index == 4 && event.pressed == true):
			if(camera_3d_ortho.current == true):
				camera_3d_ortho.size -= 1
		#Scroll down (zoom out)
		if(event.button_index == 5 && event.pressed == true):
			if(camera_3d_ortho.current == true):
				camera_3d_ortho.size += 1

func _unhandled_input(event):
	if event is InputEventMouseMotion:
		head.rotate_y(-event.relative.x * MOUSE_SENSITIVITY)
		camera.rotate_x(INVERT_MOUSE * event.relative.y * MOUSE_SENSITIVITY)
		camera.rotation.x = clamp(camera.rotation.x, deg_to_rad(-40), deg_to_rad(60))

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity -= gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity = JUMP_VELOCITY
	
	# Handle Sprint.
	if Input.is_action_pressed("sprint"):
		speed = SPRINT_SPEED
		#set_up_direction(Vector3(0, 0, 1))
		#JUMP_VELOCITY = Vector3(0, 0, 7)
	else:
		speed = WALK_SPEED
		#set_up_direction(Vector3.UP)
		#JUMP_VELOCITY = Vector3(0, 7, 0)
		
	var gamepad_input_dir = Input.get_vector("look_left", "look_right", "look_up", "look_down")
	#print("gamepad_input_dir", gamepad_input_dir)
	if(gamepad_input_dir):
		head.rotate_y(-Input.get_axis("look_left", "look_right") * CONTROLLER_X_SENSITIVITY)
		camera.rotate_x(-Input.get_axis("look_up", "look_down") * CONTROLLER_Y_SENSITIVITY)
		camera.rotation.x = clamp(camera.rotation.x, deg_to_rad(-40), deg_to_rad(60))

	# Get the input direction and handle the movement/deceleration.
	var input_dir = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	var direction = (head.transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if is_on_floor():
		if direction:
			velocity.x = direction.x * speed
			velocity.z = direction.z * speed
		else:
			velocity.x = lerp(velocity.x, direction.x * speed, delta * 7.0)
			velocity.z = lerp(velocity.z, direction.z * speed, delta * 7.0)
	else:
		velocity.x = lerp(velocity.x, direction.x * speed, delta * 3.0)
		velocity.z = lerp(velocity.z, direction.z * speed, delta * 3.0)
		
	
	# Head bob
	t_bob += delta * velocity.length() * float(is_on_floor())
	camera.transform.origin = _headbob(t_bob)
	
	# FOV
	var velocity_clamped = clamp(velocity.length(), 0.5, SPRINT_SPEED * 2)
	var target_fov = BASE_FOV + FOV_CHANGE * velocity_clamped
	camera.fov = lerp(camera.fov, target_fov, delta * 8.0)
	
	move_and_slide()


func _headbob(time) -> Vector3:
	var pos = Vector3.ZERO
	pos.y = sin(time * BOB_FREQ) * BOB_AMP
	pos.x = cos(time * BOB_FREQ / 2) * BOB_AMP
	return pos

func get_drop_position() -> Vector3:
	var direction = -camera.global_transform.basis.z
	return camera.global_position + direction

func heal(value: int) -> void:
	hp += value
