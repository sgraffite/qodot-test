extends RigidBody3D

@export var jumpHeight:float
@export var jumpTimePeak:float
@export var jumpTimeDescent:float

@onready var jumpVelocity = (2.0 * jumpHeight) / jumpTimePeak
@onready var jumpGravity = (-2.0 * jumpHeight) / (jumpTimePeak * jumpTimePeak)
@onready var fallGravity = (-2.0 * jumpHeight) / (jumpTimeDescent * jumpTimeDescent)

var gravity = Vector3.DOWN * 0.98
var speed = 5
var airSpeed = 2
var acceleration = 5
var airAcceleration = 2
var jump = false
var jumpSpeed = 25
var spin = 0.1
var direction = Vector3()
var myVelocity = Vector3(0,0,0)
var floorArray = []

var firstFall = true
var isOnFloor = false
var floorNormal = Vector3(0,0,0)
var floorPlane = Plane(Vector3.UP)

var mirrored = false

# Called when the node enters the scene tree for the first time.
func _ready():
	#custom_itegrator = true
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _integrate_forces(state):
	myVelocity = linear_velocity
	
	isOnFloor = false
	var contactCount = state.get_contact_count()
	if(contactCount > 0):
		var contactNormal = state.get_contact_local_normal(0)
		
		for i in contactCount:
			contactNormal = state.get_contact_local_normal(1)
			
			if(contactNormal.angle_to(Vector3.UP) <= 46*(PI/180)):
				isOnFloor = true
				floorNormal = contactNormal
				floorPlane.normal = floorNormal
				
	rollFloor(isOnFloor)
	
	getInput(state.step)
	
	var gravity_resistance = floorNormal if isOnFloor and !jump else Vector3.UP
	myVelocity += gravity_resistance * getGravity() * state.step
	
	if(rollFloorState):
		if(jump):
			myVelocity.y = jumpVelocity
			isOnFloor = false
	
	#for some reason a physics body will sink into the floor when landing
	#if(!jump && test_move(global_transform, myVelocity * state.step, collisionTest, 0.001, false, 1)):
		#if(collision.get_normal(0).angle_to(Vector3()) <= 46*(PI/180)):
			#transform.origin -= collisionTest.get_remainder()
			
	linear_velocity = myVelocity
	
func getInput(delta):
	var vy = myVelocity.y
	var xz = myVelocity
	if(!jump && isOnFloor):
		xz = floorPlane.project(myVelocity)

	var input_dir = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	
	if(isOnFloor):
		var x = floorPlane.intersects_segment(Vector3.RIGHT + Vector3.UP * 2, Vector3.RIGHT + Vector3.DOWN * 2).normalized()
		var z = floorPlane.intersects_segment(Vector3.BACK + Vector3.UP * 2, Vector3.BACK + Vector3.DOWN * 2).normalized()
		x *= direction.x
		z *= direction.z
		direction = (x + z).normalized()
		
	if(isOnFloor):
		if(direction.length() > 0):
			xz = xz.lerp(direction * speed, acceleration * delta)
		else:
			xz = xz.lerp(direction * speed, acceleration * 2 * delta)
	else:
		xz = xz.lerp(direction * airSpeed, airAcceleration * delta)
	
	myVelocity.x = xz.x
	if(isOnFloor && myVelocity.y < 0):
		myVelocity.y = xz.y
	myVelocity.z = xz.z
	
	jump = false
	if(Input.is_action_just_pressed("ui_select")):
		jump = true

func getGravity() -> float:
	return jumpGravity if myVelocity.y > 0 else fallGravity

#func onFloor(contactNormal) -> bool:
	#floorArray.push_back(contactNormal.angle_to(Vector3.UP) <= 46*(PI/180))
	#if(floorArray.size() > 3): floorArray.pop_front()
	#
	#var f = false
	#for e in floorArray:
		#if(e): f = e
	#return f
	
var rollFloorState = false
func rollFloor(state):
	floorArray.push_back(state)
	if(floorArray.size() > 3): floorArray.pop_front()
	
	var f = false
	for e in floorArray:
		if(e): f = e
	rollFloorState = f
	
func _unhandled_input(event):
	if(event is InputEventMouseButton and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED):
		pass
